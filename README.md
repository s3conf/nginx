# Nginx + s3conf

Nginx image ready to be used with s3conf environment configuration.

Needs sites config files mapped to `/etc/nginx/sites-enabled` and static files mapped to `/app/static`.

## References

Image comes from `https://hub.docker.com/r/phusion/baseimage/`.
Dockerfile inspired on `https://github.com/27Bslash6/docker/tree/master/nginx-pagespeed`.
Most of the content of this folder comes from `https://github.com/h5bp/server-configs-nginx`. 
